package droneproject.android.knu.com.easyquad.model;

import java.io.Serializable;

public class Orders implements Serializable{
    public String getOrderText() {
        return orderText;
    }

    public void setOrderText(String orderText) {
        this.orderText = orderText;
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    String orderText;
    String data;
    String throttleData;
    public Orders(){
        data = null;
        throttleData = null;
    }
    public void setData(String data){
        this.data = data;
    }

    public void setOrders(String orderText, String data, String throttleData){
        this.orderText = orderText;
        this.data = data;
        this.throttleData = throttleData;
    }

    public String getThrottleData() {
        return throttleData;
    }

    public void setThrottleData(String throttleData) {
        this.throttleData = throttleData;
    }

    public String getData() {
        return data;
    }

}