package droneproject.android.knu.com.easyquad.Joystick;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by Arron on 2015-04-30.
 */
public class JoystickView extends View{

    public static final String JOYSTICK_VIEW_TAG = "JOYSTICK_VIEW_TAG";
    public static final String JOYSTICK_POSITION = "JOYSTICK_POSITION";
    protected OnJoystickMovedListener onJoystickMovedListener;
    protected int xPosition;
    protected int yPosition;
    protected double centerX;
    protected double centerY;
    protected int joystickRadius;
    protected int buttonRadius;

    public JoystickView(Context context) {
        super(context);
        init(context);
    }

    public JoystickView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    protected void init(Context context){
        xPosition = 0;
        yPosition = 0;
        centerX = 0;
        centerY = 0;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int d = Math.min(widthMeasureSpec, heightMeasureSpec);
        Log.d(JOYSTICK_VIEW_TAG,"onMeasure parameters" + widthMeasureSpec );
        setMeasuredDimension(d, d);
    }

    private int measure(int measureSpec) {
        int result = 0;

        // Decode the measurement specifications.
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.UNSPECIFIED) {
            // Return a default size of 200 if no bounds are specified.
            result = 200;
        } else {
            // As you want to fill the available space
            // always return the full available bounds.
            result = specSize;
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        centerX = getWidth() / 2;
        centerY = getHeight() / 2;
    }

    public void setOnJoystickMovedListener(OnJoystickMovedListener onJoystickMovedListener){
        this.onJoystickMovedListener = onJoystickMovedListener;
    }
}
