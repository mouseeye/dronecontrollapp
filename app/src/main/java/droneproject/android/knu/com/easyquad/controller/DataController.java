package droneproject.android.knu.com.easyquad.controller;

import android.content.Context;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import droneproject.android.knu.com.easyquad.Joystick.JoystickView;
import droneproject.android.knu.com.easyquad.Joystick.OnJoystickMovedListener;

/**
 * Created by Arron on 2015-05-03.
 */
public class DataController {
    JoystickView rightJoystickView;
    JoystickView leftJoystickView;

    String throtleData = "0";
    String prevX = "3/0";
    String prevY = "4/0";
    String orderText = "";
    OrderController orderController;

    public DataController(JoystickView left, JoystickView right) {
        orderController = new OrderController();
        setJoystickListener(left, right);
    }

    private void setJoystickListener(JoystickView left, JoystickView right) {
        leftJoystickView = left;
        rightJoystickView = right;
        setLeftJoystickMovedListener();
        setRightJoystickMovedListener();
    }

    private void setLeftJoystickMovedListener() {
        leftJoystickView.setOnJoystickMovedListener(new OnJoystickMovedListener() {

            @Override
            public void onMoved(int x, int y) {
                String levelX = getLevelForX(x - 83);
                String levelY = getLevelForY(y - 83);

                if (levelX != prevX || levelY != prevY) {

                    prevX = levelX;
                    prevY = levelY;

                    //throtle이랑 합해서 보내야지...
                    //그대로 보내면 안되고... 해석을 해야됨...해석하는 class OrderController에 구현..
                    Log.d("Test", "order text event in : " + orderText);
                    orderController.sendOrderData(orderText, prevX, throtleData);
                    orderController.sendOrderData(orderText, prevY, throtleData);
                }
            }
        });
    }

    private String getLevelForX(int x) {

        if (x != 247) {
            if (x >= 0 && x < 123) {
                return "2/2";
            } else if (x >= 123 && x < 247) {
                return "2/1";
            } else if (x > 247 && x < 370) {
                return "4/1";
            } else if (x >= 370 && x < 494) {
                return "4/2";
            }
        }
        return "2/0";   //center
    }

    private String getLevelForY(int y) {

        if (y != 247) {
            if (y >= 0 && y < 123) {
                return "1/2";
            } else if (y >= 123 && y < 247) {
                return "1/1";
            } else if (y > 247 && y < 370) {
                return "3/1";
            } else if (y >= 370 && y < 494) {
                return "3/2";
            }
        }
        return "1/0";   //center
    }

    private void setRightJoystickMovedListener() {
        rightJoystickView.setOnJoystickMovedListener(new OnJoystickMovedListener() {
            @Override
            public void onMoved(int x, int y) {

                String throtle = getThrotle(y - 83);
                if(throtle != throtleData){

                    //이전 속도와 다를시 이벤트 적용...

                    throtleData = throtle;

                    orderController.sendOrderData(orderText, " /1", throtleData);
                }

            }
        });
    }

    private String getThrotle(int y) {
        if (y != 494) {
            if (y >= 0 && y < 49) {
                return "10";
            } else if (y >= 49 && y < 98) {
                return "9";
            } else if (y >= 98 && y < 148) {
                return "8";
            } else if (y >= 148.2 && y < 197) {
                return "7";
            } else if (y >= 197 && y < 247) {
                return "6";
            } else if (y >= 247 && y < 296) {
                return "5";
            } else if (y >= 296 && y < 345) {
                return "4";
            } else if (y >= 345 && y < 395) {
                return "3";
            } else if (y >= 395 && y < 444) {
                return "2";
            } else if (y >= 444 && y < 494) {
                return "1";
            }
        }
        return "0";   //stop
    }

    public void setOrderText(int RSSI) {
        Log.d("Test", "setOrderText in" + RSSI);
        if (RSSI <= -80) {
            this.orderText = "Recovery";
        } else if (RSSI > -80) {
            this.orderText = "Order";
        } else
            ;
    }
}
