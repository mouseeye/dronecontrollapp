package droneproject.android.knu.com.easyquad.Joystick;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import droneproject.android.knu.com.easyquad.Joystick.JoystickView;

/**
 * Created by Arron on 2015-04-30.
 */
public class LeftJoystickView extends JoystickView {

    public static final String JOYSTICK_VIEW_TAG = "JOYSTICK_VIEW_TAG";
    private Paint mainCircle;
    private Paint secondaryCircle;
    private Paint button;

    public LeftJoystickView(Context context) {
        super(context);
        init(context);
    }

    public LeftJoystickView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    protected void init(Context context){
        super.init(context);
        mainCircle = new Paint(Paint.ANTI_ALIAS_FLAG);
        mainCircle.setColor(Color.argb(150, 255, 80, 4));
        mainCircle.setStyle(Paint.Style.STROKE);
        mainCircle.setStrokeWidth(30);
        mainCircle.setAntiAlias(true);

        secondaryCircle = new Paint(Paint.ANTI_ALIAS_FLAG);
        secondaryCircle.setAntiAlias(true);
        secondaryCircle.setColor(Color.argb(50, 255, 255, 255));
        secondaryCircle.setStyle(Paint.Style.STROKE);
        secondaryCircle.setStrokeWidth(10);

        button = new Paint(Paint.ANTI_ALIAS_FLAG);
        button.setAntiAlias(true);
        button.setColor(Color.argb(150, 255, 80, 4));
        button.setStyle(Paint.Style.FILL);
    }

    private int measure(int measureSpec) {
        int result = 0;

        // Decode the measurement specifications.
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.UNSPECIFIED) {
            // Return a default size of 200 if no bounds are specified.
            result = 200;
        } else {
            // As you want to fill the available space
            // always return the full available bounds.
            result = specSize;
        }
        return result;
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        // before measure, get the center of view
        xPosition = (int) getWidth() / 2;
        yPosition = (int) getWidth() / 2;
        int d = Math.min(xNew, yNew);
        buttonRadius = (int) (d / 2 * 0.25);
        joystickRadius = (int) (d / 2 * 0.75);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle((int) centerX, (int) centerY, joystickRadius, mainCircle);
        canvas.drawCircle((int)centerX, (int)centerY, joystickRadius/2, secondaryCircle);
        canvas.drawCircle(xPosition, yPosition, buttonRadius, button);
        //Log.d("Test", "X ��ǥ : " + xPosition);
        //Log.d("Test", "Y ��ǥ : " + yPosition);
    }

    public boolean onTouchEvent(MotionEvent event){
        xPosition = (int) event.getX();
        yPosition = (int) event.getY();
        int movingValue = event.getAction();
        double abs = Math.sqrt((xPosition-centerX)*(xPosition-centerX)+(yPosition-centerY)*(yPosition-centerY));
        if(abs>joystickRadius){
            xPosition = (int)((xPosition-centerX)*joystickRadius / abs +centerX);
            yPosition = (int)((yPosition-centerY)*joystickRadius / abs +centerY);
        }
        if(movingValue == MotionEvent.ACTION_DOWN || movingValue == MotionEvent.ACTION_MOVE){

            if(onJoystickMovedListener!= null){
                onJoystickMovedListener.onMoved(xPosition, yPosition);
            }
        }
        invalidate();
        if(movingValue == MotionEvent.ACTION_UP){
            xPosition = (int) centerX;
            yPosition = (int) centerY;
            if(onJoystickMovedListener != null) {
                onJoystickMovedListener.onMoved(xPosition, yPosition);
            }
            invalidate();

        }
        return true;

    }
}
