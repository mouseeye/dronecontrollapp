package droneproject.android.knu.com.easyquad.model;

/**
 * Created by Arron on 2015-05-03.
 */
public class HostInfo {
    String hostIp;
    int port;

    public HostInfo(String hostIp, int port) {
        this.hostIp = hostIp;
        this.port = port;
    }


    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
