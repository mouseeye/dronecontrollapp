package droneproject.android.knu.com.easyquad.Joystick;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import droneproject.android.knu.com.easyquad.Joystick.JoystickView;
import droneproject.android.knu.com.easyquad.Joystick.OnJoystickMovedListener;

/**
 * Created by Arron on 2015-04-30.
 */
public class RightJoystickView extends JoystickView {

    public static final String JOYSTICK_VIEW_TAG = "JOYSTICK_VIEW_TAG";
    private Paint mainCircle;
    private Paint button;

    public RightJoystickView(Context context) {
        super(context);
        init(context);
    }

    public RightJoystickView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    protected void init(Context context){
        super.init(context);
        mainCircle = new Paint(Paint.ANTI_ALIAS_FLAG);
        mainCircle.setColor(Color.argb(150, 255, 80, 4));
        mainCircle.setStyle(Paint.Style.STROKE);
        mainCircle.setStrokeWidth(30);
        mainCircle.setAntiAlias(true);

        button = new Paint(Paint.ANTI_ALIAS_FLAG);
        button.setAntiAlias(true);
        button.setColor(Color.argb(180, 70, 140, 255));
        button.setStyle(Paint.Style.FILL);

    }

    private int measure(int measureSpec) {
        int result = 0;

        // Decode the measurement specifications.
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.UNSPECIFIED) {
            // Return a default size of 200 if no bounds are specified.
            result = 200;
        } else {
            // As you want to fill the available space
            // always return the full available bounds.
            result = specSize;
        }
        return result;
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        // before measure, get the center of view
        xPosition = (int) getWidth() / 2;
        yPosition = (int) 577;
        int d = Math.min(xNew, yNew);
        buttonRadius = (int) (d / 2 * 0.25);
        joystickRadius = (int) (d / 2 * 0.75);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle((int) centerX, (int) centerY, joystickRadius, mainCircle);
        canvas.drawCircle(xPosition, yPosition, buttonRadius, button);
    }

    public boolean onTouchEvent(MotionEvent event){
        xPosition = (int) centerX;
        yPosition = (int) event.getY();
        int movingValue = event.getAction();

        if(yPosition>joystickRadius+centerY){
            yPosition = (int) (centerY+joystickRadius);
        }else if(yPosition<(centerY-joystickRadius)){
            yPosition = (int)centerY-joystickRadius;
        }
        if(movingValue == MotionEvent.ACTION_DOWN || movingValue == MotionEvent.ACTION_MOVE){
            if(onJoystickMovedListener!= null){
                onJoystickMovedListener.onMoved(xPosition, yPosition);
            }
        }
        invalidate();
        return true;
    }

    public void setOnJoystickMovedListener(OnJoystickMovedListener onJoystickMovedListener){
        this.onJoystickMovedListener = onJoystickMovedListener;
    }
}
