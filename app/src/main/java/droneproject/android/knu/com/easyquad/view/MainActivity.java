package droneproject.android.knu.com.easyquad.view;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.Vibrator;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;

import droneproject.android.knu.com.easyquad.Joystick.LeftJoystickView;
import droneproject.android.knu.com.easyquad.Joystick.RightJoystickView;
import droneproject.android.knu.com.easyquad.R;
import droneproject.android.knu.com.easyquad.controller.DataController;


public class MainActivity extends Activity {

    ImageButton captureButton;
    ImageButton settingButton;
    ImageButton galleryButton;
    DataController dataController;
    LeftJoystickView leftJoystickView;
    RightJoystickView rightJoystickView;
    RTSPVideoView videoView;
    int _rssi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init(){

        videoView = (RTSPVideoView) findViewById(R.id.videoView);
        leftJoystickView = (LeftJoystickView) findViewById(R.id.left_stick);
        rightJoystickView = (RightJoystickView) findViewById(R.id.right_stick);
        MediaController mediaController = new MediaController(this);
        videoView.setMediaController(mediaController);
        videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/raw/quad"));
        videoView.start();
        settingButton = (ImageButton) findViewById(R.id.settings);
        settingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), SettingActivity.class);
                startActivity(i);
            }
        });

        galleryButton = (ImageButton) findViewById(R.id.pictures);
        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), PicturesActivity.class);
                startActivity(i);
            }
        });

        captureButton = (ImageButton) findViewById(R.id.capture);
        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                mediaMetadataRetriever.setDataSource(getApplicationContext(), Uri.parse("android.resource://" + getPackageName() + "/raw/quad"));
                int currentPosition = videoView.getCurrentPosition();
                Bitmap bmFrame = mediaMetadataRetriever.getFrameAtTime(currentPosition * 1000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
                if (bmFrame == null) {
                    Toast.makeText(getApplicationContext(), "bmFrame == null!",Toast.LENGTH_LONG).show();
                } else {
                    FileOutputStream fos;
                    try {
                        Log.d("Test", "Capture 안");
                        File path = new File(Environment.getExternalStorageDirectory(), "QuadPictures");
                        if(!path.exists()){
                        path.mkdirs();}
                        fos = new FileOutputStream(path +"/"+ System.currentTimeMillis()+".jpeg");
                        bmFrame.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        Log.d("Test", "캡쳐 성공");
                        Toast.makeText(getApplicationContext(), Environment.getExternalStorageDirectory().toString() + "/" + System.currentTimeMillis()+".jpeg", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Log.d("Test", " " + e);
                    e.printStackTrace();
                }
                }
//
// videoView.buildDrawingCache();
//                Bitmap bitmap = videoView.getDrawingCache();
//                FileOutputStream fos;
//                try {
//                    Log.d("Test", "Capture 안");
//                    File path = new File(Environment.getExternalStorageDirectory(), "QuadPictures");
//                    if(!path.exists()){
//                        path.mkdirs();
//                    }
//                    fos = new FileOutputStream(path +"/"+ System.currentTimeMillis()+".jpeg");
//                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
//                    Log.d("Test", "캡쳐 성공");
//                    Toast.makeText(getApplicationContext(), Environment.getExternalStorageDirectory().toString() + "/" + System.currentTimeMillis()+".jpeg", Toast.LENGTH_LONG).show();
//                } catch (Exception e) {
//                    Log.d("Test", " " + e);
//                    e.printStackTrace();
//                }
                //Toast.makeText(getApplicationContext(), "화면캡쳐 성공", Toast.LENGTH_LONG).show();
            }
        });
        dataController = new DataController(leftJoystickView, rightJoystickView);
        registerReceiver(rssiReceiver, new IntentFilter(WifiManager.RSSI_CHANGED_ACTION));
    }

    private BroadcastReceiver rssiReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Vibrator vr = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            WifiManager wman = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = (WifiInfo) wman.getConnectionInfo();
            _rssi = wifiInfo.getRssi();
            if (_rssi <= -80) {
                Toast.makeText(getApplication(), "WIFI 수신강도가 약합니다!", Toast.LENGTH_LONG).show();
                long pattern[] = {100, 2000};
                vr.vibrate(pattern, 0);
            } else {
                vr.cancel();
            }
            dataController.setOrderText(_rssi);
            Log.d("RSSI", "RSSSI : " + _rssi);
        }
    };
}