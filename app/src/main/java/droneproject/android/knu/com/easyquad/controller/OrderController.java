package droneproject.android.knu.com.easyquad.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.StringTokenizer;
import droneproject.android.knu.com.easyquad.model.Orders;

/**
 * Created by Arron on 2015-06-18.
 */
public class OrderController {

    Socket socket;
    ObjectOutputStream os;


    public OrderController() {
        setConnection();
    }

    private void setConnection() {
        try {
            socket = ConnectionController.getSocket();
            os = ConnectionController.getObjectOutputStream();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendOrderData(String orderText, String left, String right){

            StringTokenizer tokenizer = new StringTokenizer(left, "/");
            String sign = tokenizer.nextToken();
            int count = Integer.valueOf(tokenizer.nextToken());

            for(int i=0; i<count; i++){
                Orders orders = new Orders();
                orders.setOrders(orderText, sign, right);

                try {
                    os.reset();
                    os.writeObject(orders);
                    os.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

    }
}
