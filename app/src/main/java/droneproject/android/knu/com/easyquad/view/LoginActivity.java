package droneproject.android.knu.com.easyquad.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import droneproject.android.knu.com.easyquad.controller.ConnectionController;
import droneproject.android.knu.com.easyquad.model.HostInfo;
import droneproject.android.knu.com.easyquad.R;


public class LoginActivity extends Activity {

    HostInfo info;
    EditText editTextPortNum;
    EditText editTextIP;
    TextView textViewComment;
    boolean connStat = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        editTextIP = (EditText)findViewById(R.id.editTextIP);
        editTextPortNum = (EditText)findViewById(R.id.editTextPortNum);

        editTextIP.setText("172.16.0.6");
        editTextPortNum.setText("8080");
        textViewComment = (TextView)findViewById(R.id.textViewComent);
        textViewComment.setText("1. Insert the ip address and port number\n" +
                "2. ip example : 192.x.xx.xxx");
    }

    public void onConnectButtonClicked(View view) {
        String ip = editTextIP.getText().toString();
        String portNum = editTextPortNum.getText().toString();
        if (!ip.equals("") || !portNum.equals("")) {
            info = new HostInfo(ip, Integer.parseInt(portNum));
            ConnectionController.setHostInfo(info);
                if (ConnectionController.getSocket()!=null || ip.equals("fakeconnection")) {
                    Intent i = new Intent(this, MainActivity.class);
                    startActivity(i);
                    this.finish();
                } else {
                    createAlertDialog("연결실패", "연결실패", "확인");
                    return;
                }
            }
        else
            createAlertDialog("경고", "IP와 Port번호를 입력해 주세요", "확인");
        }

    private void createAlertDialog(String title, String message, String btnName){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(btnName, null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
