package droneproject.android.knu.com.easyquad.Joystick;

/**
 * Created by Arron on 2015-04-30.
 */
public interface OnJoystickMovedListener {
    public void onMoved(int x, int y);
}
