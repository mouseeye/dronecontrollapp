package droneproject.android.knu.com.easyquad.controller;

import android.util.Log;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

import droneproject.android.knu.com.easyquad.model.HostInfo;

/**
 * Created by Arron on 2015-05-03.
 */
public class ConnectionController{
    private static Socket socket;
    private static HostInfo hostInfo;
    private static ObjectOutputStream objectOutputStream;
    public static Socket getSocket(){
        if(hostInfo != null) {
            if (socket != null) {
                return socket;
            } else {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            socket = new Socket(hostInfo.getHostIp(), hostInfo.getPort());
                            objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
                long start = System.currentTimeMillis();
                while(true){
                    if(System.currentTimeMillis()-start > 10000 || socket !=null){
                        return socket;
                    }
                }
            }
        }
        return null;
    }

    public static ObjectOutputStream getObjectOutputStream(){
        return objectOutputStream;
    }
    public static void setHostInfo(HostInfo info){
        hostInfo = info;
    }

    public static void closeSocket(){
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Thread안에 안넣음 나중에 실험
    }
}
