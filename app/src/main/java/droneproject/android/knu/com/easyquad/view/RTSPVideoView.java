package droneproject.android.knu.com.easyquad.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Display;
import android.view.WindowManager;
import android.widget.VideoView;

/**
 * Created by Arron on 2015-05-24.
 */
public class RTSPVideoView extends VideoView {

    public RTSPVideoView(Context context) {
        super(context);
    }

    public RTSPVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Display dis = ((WindowManager)getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        setMeasuredDimension(dis.getWidth(), dis.getHeight());
    }

    private void init(){
    }
}
