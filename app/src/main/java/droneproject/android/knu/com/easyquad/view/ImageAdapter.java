package droneproject.android.knu.com.easyquad.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;

import droneproject.android.knu.com.easyquad.R;

/**
 * Created by Arron on 2015-06-20.
 */

class ImageAdapter extends PagerAdapter {
    Context context;
    File []files;

    ImageAdapter(Context context){
        this.context=context;
        getFileList();
    }

    private void getFileList(){
        File path = new File(Environment.getExternalStorageDirectory(), "QuadPictures");
        if(!path.exists()){
            path.mkdirs();
        }
        files = path.listFiles();
    }

    @Override
    public int getCount() {
        return files.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
        int padding = context.getResources().getDimensionPixelSize(R.dimen.padding_medium);
        imageView.setPadding(padding, padding, padding, padding);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        BitmapFactory.Options options =  new BitmapFactory.Options();
        options.inSampleSize = 0;
        Bitmap bitmap = BitmapFactory.decodeFile(String.valueOf(files[position]), options);
        imageView.setImageBitmap(bitmap);
        //imageView.setImageResource(files[position]);
        ((ViewPager) container).addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }
}
