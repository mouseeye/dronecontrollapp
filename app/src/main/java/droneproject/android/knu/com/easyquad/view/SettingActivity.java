package droneproject.android.knu.com.easyquad.view;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.astuetz.PagerSlidingTabStrip;

import droneproject.android.knu.com.easyquad.R;
import droneproject.android.knu.com.easyquad.Settings.ConnectionSettingsFragment;
import droneproject.android.knu.com.easyquad.Settings.ModeSettingsFragment;
import droneproject.android.knu.com.easyquad.Settings.VideoSettingsFragment;

/**
 * Created by Arron on 2015-05-11.
 */
public class SettingActivity extends FragmentActivity {

    ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_settings);
        pager = (ViewPager) findViewById(R.id.viewPager);

        pager.setAdapter(new SettingsPageAdapter(getSupportFragmentManager()));
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(pager);
        setTabPreferences(tabs);
    }

    public void setTabPreferences(PagerSlidingTabStrip tabs){
        tabs.setIndicatorColor(Color.argb(180, 70, 140, 255));
        tabs.setUnderlineColor(Color.argb(255, 213, 213, 213));
        tabs.setDividerColor(Color.argb(255, 213, 213, 213));
    }

    class SettingsPageAdapter extends FragmentPagerAdapter{

        String []title = {"   Connection   settings  ", "             Mode   Settings          ", "            Video   Settings          "};

        public SettingsPageAdapter(android.support.v4.app.FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0){
                return ConnectionSettingsFragment.newInstance();
            }else if(position == 1){
                return ModeSettingsFragment.newInstance("dssfa", "asdfasdf");
            }else 
            return VideoSettingsFragment.newInstance("safdsa", "asdfasdf");
        }

        @Override
        public int getCount() {
            return title.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return title[position];
        }
    }
}
